import configparser
import os
import traceback


#Create configtemplate if theres no configfile
if not (os.path.exists("config.cfg")):
    with open("config.cfg", 'w', encoding='utf-8') as f:
        f.write(r"""[Paths]
video_path = ...
east_path = resurser\frozen_east_text_detection.pb
dict_unknown_words = video-text-detection\Dict\unknownWords.txt
custom_dictionary_path = video-text-detection/Dict/customDictionaryWords.txt

#All numbers range from 0 - 1
[TextDetection]
enchant_dict = sv_SE
VideoSearchLimitX1 = 0
VideoSearchLimitY1 = 0.6
VideoSearchLimitX2 = 1
VideoSearchLimitY2 = 1
# How many of the previous frames the current text should be compared to.
# At least one of the previous frames must match to (buffersimilarity*100) % to pass)
# Can take values between 0.01 - 0.89
BufferSimilarity = 0.45
# accepted values 2 - 10
FrameToFrameBufferSize = 3
# How many percentage of the highest scored word each word must have to "pass"
WordScoreTreshhold = 0.3
        """)
    print('config.cfg created in repo\'s root dicectory. Replace "..." with your paht(s)')
    exit(0)

config = configparser.ConfigParser(comment_prefixes='/', allow_no_value=True)
config.read('config.cfg')

######## Getters #########
def get_video_path():
    path = __get('Paths','video_path')
    if (os. path.exists(path)):
        return path
    else:
        raise Exception(f"Could not find the the videofile at given path\nPath: {path}")

def get_east_path():
    return __get('Paths', 'east_path')

def get_custom_dictionary_path():
    return __get('Paths', 'custom_dictionary_path')

def get_dict_unknownword_path():
    return __get('Paths', 'dict_unknown_words')

def get_enchant_dict():
    return __get('TextDetection', 'enchant_dict')

def get_video_searchlimit():
    x1 = float(__get('TextDetection', 'VideoSearchLimitX1'))
    y1 = float(__get('TextDetection', 'VideoSearchLimitY1'))
    x2 = float(__get('TextDetection', 'VideoSearchLimitX2'))
    y2 = float(__get('TextDetection', 'VideoSearchLimitY2'))    
    return (x1, y1, x2, y2)

def get_buffer_similarity():
    return float(__get('TextDetection', 'BufferSimilarity'))

def get_frame_to_frame_buffer_size():
    return int(__get('TextDetection', 'FrameToFrameBufferSize'))

def get_wordscore_threshold():
    return float(__get('TextDetection', 'WordScoreTreshhold'))

def __get(section, part):
    try:
        return config[section][part]
    except:
        raise Exception(f"could not find item in configfile. \nSection: {section} \nPart: {part}")

######## Setters #########
def __set(section, part, value):
    try:
        config.set(section, part, value)
    except:
        raise Exception(f"the setting ({section}, {part}) could not be found. Incorrect formated setting")

def apply_configuration(option_list):
    for option in option_list:
        if(option.section == "preset"):
            if option.part == "lower":
                __set("TextDetection", "videosearchlimitx1", "0")
                __set("TextDetection", "videosearchlimity1", "0.6")
                __set("TextDetection", "videosearchlimitx2", "0.9")
                __set("TextDetection", "videosearchlimity2", "1")
            elif option.part == "upper":
                __set("TextDetection", "videosearchlimitx1", "0")
                __set("TextDetection", "videosearchlimity1", "0")
                __set("TextDetection", "videosearchlimitx2", "0.9")
                __set("TextDetection", "videosearchlimity2", "0.3")
            elif option.part == "full":
                __set("TextDetection", "videosearchlimitx1", "0")
                __set("TextDetection", "videosearchlimity1", "0")
                __set("TextDetection", "videosearchlimitx2", "0.9")
                __set("TextDetection", "videosearchlimity2", "1")
            else:
                raise Exception(f"Preset ({option.part}) doesn't exist")
        elif config.has_option(option.section, option.part):
            __set(option.section, option.part, option.value)
        else:
            raise Exception(f"The setting ({option.section}, {option.part}) could not be found.")
    with open("config.cfg", "w") as f:
        config.write(f)

def preset_searchLimit_upper():
    pass

def preset_searchLimit_lower():
    pass

def preset_searchLimit_fullscreen():
    pass
    

########################## Custom words, Dictionary ##################################
if not (os.path.exists(get_custom_dictionary_path())):
    with open(get_custom_dictionary_path(), 'w', encoding='utf-8') as f:
        f.write("#Write a list with words to be added to the dictionary\n")
        f.write("#Each word should be on a new line\n")
        f.write("#The words should be in lower case")
    
# Call from text_filter
def init_customWords(dictionary):
    try:
        with open(get_custom_dictionary_path(), 'r', encoding='utf-8') as f:
            custom_words = set(f.read().splitlines())
        for word in custom_words:
            if not (word.__contains__('#')):
                dictionary.add_to_session(word)
    except:
        traceback.print_exc()
        raise Exception("Could not add custom words to dictionary")
    
      