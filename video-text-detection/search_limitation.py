import cv2
import numpy as np

class searchLimitation:
    # x, x2, y, y2 is percentage values. 0 is the left top corner. x=1 the bottom. y=1 the right.
    def __init__(self, x, y, x2, y2, cv2Image):
        self.__x = x
        self.__y = y
        self.__x2 = x2
        self.__y2 = y2
     
        if cv2Image is None or not isinstance(cv2Image, np.ndarray):
            raise Exception("Invalid values")

        self.__origimage = cv2Image
        (self.__h, self.__w) = self.__origimage.shape[:2]

        self.px = 0
        self.py = 0
        self.px2 = self.__w
        self.py2 = self.__h
        self.calculateSearchArea()

    def __init__(self, x, y, x2, y2):
        self.__x = x
        self.__y = y
        self.__x2 = x2
        self.__y2 = y2

        self.__origimage = None
        self.__h = None
        self.__w = None

        self.px = 0
        self.py = 0
        self.px2 = 0
        self.py2 = 0

    def SetImage(self, cv2Image):
        if cv2Image is None or not isinstance(cv2Image, np.ndarray):
            raise Exception("set image wassn't in cv2 image format")
        
        self.__origimage = cv2Image

        (self.__h, self.__w) = self.__origimage.shape[:2]

        self.px = 0
        self.py = 0
        self.px2 = self.__w
        self.py2 = self.__h
        self.calculateSearchArea()


    def changeSearchArea(self, x, y, x2, y2):
        self.__x = x
        self.__y = y
        self.__x2 = x2
        self.__y2 = y2
        self.calculateSearchArea()


    def printValues(self, message=""):
        print(f"\n{message}")
        print(f"Image x(width) bounds:  {self.px} - {self.px2} \n")
        print(f"Image y(height) bounds:  {self.py} - {self.py2}\n\n")  


    def calculateSearchArea(self):
        self.px = (self.__w*self.__x)
        self.py = (self.__h*self.__y)
        self.px2 = (self.__w*self.__x2)
        self.py2 = (self.__h*self.__y2)

    def getCropImage(self):
        return self.__origimage[int(self.py):int(self.py2), int(self.px):int(self.px2)]
