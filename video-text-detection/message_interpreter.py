import re
from configreader import apply_configuration

class message_object():
    def __init__(self):
        self.message_type = None
        self.section = None
        self.part = None
        self.value = None

    def validate(self):
        if(self.message_type == "config" and self.section and self.part and self.value):
            return True
        elif(self.message_type == "sub" and self.value):
            return True
        else:
            return False

    def set_section(self, section):
        self.section = section 

    def set_part(self, part):
        self.part = part 
    
    def set_value(self, value):
        self.value = value 

    def set_message_type(self, message_type):
        self.message_type = message_type

    def to_string(self):
        str1 = f"""
Section:    {self.section}
Part:       {self.part}
Value:      {self.value}"""
        return str1


def interpret_message(input, text_detection_instance):
    messages = list()
    if __is_multiline(input):
        split_messages = re.split("\n", input)
        for message in split_messages:
            messages.append(__split_message_from_tag(message))
    else:
        messages.append(__split_message_from_tag(input))
    
    for message in messages:
        if __is_config(message):
            # Change setting
            print(f"received config")
            apply_configuration(messages)
            text_detection_instance.reload_searchbox_area()
            break
        else:
            # received subtitles
            print(f"reveiced subtitle: {message.value}")


def __is_multiline(message):
    return re.search("\n", message)


# separate meta-data from message"
def __split_message_from_tag(message):
    temp_result = re.split("[#]{2}", message)
    # if len(temp_result):
    temp_result.pop(0)
    mess_object = message_object()
    # If it's a config-message, it will remove the '#' between the messagetag and what
    # setting to change. If it's a sub-message, nothing will happen
    print(temp_result[0])
    if(re.search("config", temp_result[0])):
        message_type, section, part = re.split("[#]", temp_result[0])

        mess_object.set_message_type(message_type)
        mess_object.set_section(section)
        mess_object.set_part(part)
        mess_object.set_value(temp_result[1])
        if(mess_object.validate()):
            return mess_object
    elif(re.search("sub", temp_result[0])):
        mess_object.set_message_type(temp_result[0])
        mess_object.set_value(temp_result[1])
        if(mess_object.validate()):
            return mess_object
        else:
            raise Exception("message_object missing required attributes")
    return None


def __is_config(config_object):
    if(config_object.message_type == "config"):
        return True
    else:
        return False
