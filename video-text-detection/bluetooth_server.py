import bluetooth
import os
import time
import threading
from message_interpreter import interpret_message
import text_detection_video_thread
import re

class bluetooth_connection():
    def __init__(self):
        # os.system("sudo bash /home/pi/Desktop/BTPrep.sh")
        # time.sleep(0.5)

        self.uuid = "f30d78b2-2c2f-49a1-be90-7c0b98c3202c"
        self.client_socket = None
        self.__is_receiving = None
        self.__text_detection_instance = None



    def send(self, data):
        print(f"sending: {data}")
        self.client_socket.send(data)


    def __receive(self):
        try:
            while self.__is_receiving:
                data = self.client_socket.recv(1024)
                if not data:
                    raise OSError
                # Do something with the data
                data = re.split("b'", str(data))
                data.pop(0)
                data = data[0]
                interpret_message(data, self.__text_detection_instance)
        except OSError:
            pass


    def establish_connection(self):
        # Create socket
        self.server_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self.server_socket.bind(("", bluetooth.PORT_ANY))
        self.server_socket.listen(1)

        port = self.server_socket.getsockname()[1]

        bluetooth.advertise_service(
            self.server_socket, 
            "ocr-aid-server",
            service_id = self.uuid, 
            service_classes = [ self.uuid, bluetooth.SERIAL_PORT_CLASS ], 
            profiles = [ bluetooth.SERIAL_PORT_PROFILE ])


        print(f"RFCOMM connection open on channel {port}")
        self.client_socket, client_info = self.server_socket.accept()
        self.server_socket.close() # this will disable the advertise_service
        print("Accepted connection from", client_info)

        self.__is_receiving = True

        self.receive_thread = threading.Thread(target=self.__receive)
        self.receive_thread.daemon = True
        self.receive_thread.start()

        self.test_connectrion_thread = threading.Thread(target=self.__test_connection)
        self.test_connectrion_thread.daemon = True
        self.test_connectrion_thread.start()
        return True





    def close_connection(self):
        self.__is_receiving = False
        if self.client_socket:
            print("Disconnecting...")
            self.server_socket.close()
            self.client_socket.close()
            self.client_socket = None
            print("Disconnected")
        else:
            print("There are no connections to close")
    

    def __test_connection(self):
        while self.__is_receiving:
            try:
                self.client_socket.getpeername()
            except:
                threading.Thread(target=self.__restore_connection).start()
                break
            time.sleep(10)
    
    def __restore_connection(self):
        self.close_connection()
        self.__init__()
        self.establish_connection()

    # To be able to change search area during runtime, the textDetectionThread is needed. 
    def prep_remote_configuration(self, instance):
        self.__text_detection_instance = instance
       

