package com.example.myapplication;

import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.ViewModelProvider;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;

public class SettingsFragment extends PreferenceFragmentCompat {

    BluetoothService bluetoothService;
    String startSearchLimitMode;
    String newSearchLimitMode;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
        BluetoothViewModel bvm = new ViewModelProvider(requireActivity()).get(BluetoothViewModel.class);
        bluetoothService = bvm.getBluetoothService();

        SeekBarPreference sbp = findPreference(getString(R.string.speechRateKey));
        setSpeechRateSummary(sbp, convertValueToSpeechRate(sbp.getValue()));

        sbp.setOnPreferenceChangeListener(((preference, newValue) -> {
            setSpeechRateSummary(sbp, convertValueToSpeechRate((int)newValue));
            return true;
        }));

        ListPreference lp = findPreference(getString(R.string.searchLimitModeKey));
        startSearchLimitMode = lp.getValue();
        newSearchLimitMode = startSearchLimitMode;

        setSearchLimitModeSummary(lp, startSearchLimitMode);

        lp.setOnPreferenceChangeListener((preference, newValue) -> {
            newSearchLimitMode = newValue.toString();
            setSearchLimitModeSummary(lp, newSearchLimitMode);
            return true;
        });

    }

    private float convertValueToSpeechRate(int speechRateValue) {
        return speechRateValue/10f;
    }

    private void setSpeechRateSummary(SeekBarPreference preference, float speechRate) {
        String customSpeechRateSummary = getString(R.string.customSpeechRateSummary);

        float defaultSpeechRate = Float.parseFloat(getString(R.string.defaultSpeechRateValue));
        if(speechRate == defaultSpeechRate) {
            preference.setSummary(customSpeechRateSummary + " " + speechRate + " " + getString(R.string.customDefaultSpeechRateEnding));
        } else {
            preference.setSummary(customSpeechRateSummary + " " + speechRate + " " + getString(R.string.customNonDefaultSpeechRateEnding) + " " + defaultSpeechRate + ")");
        }
    }

    private void setSearchLimitModeSummary(ListPreference preference, String searchLimitMode) {
        if(searchLimitMode.equals("0")) {
            preference.setSummary(preference.getEntries()[Integer.parseInt(searchLimitMode)] + " " + getString(R.string.defaultSearchLimitModeEnding));
        } else {
            preference.setSummary(preference.getEntries()[Integer.parseInt(searchLimitMode)] + " " + getString(R.string.nonDefaultSearchLimitModeEnding) + " " + preference.getEntries()[0] + ")");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(!startSearchLimitMode.equals(newSearchLimitMode)) {
            switch (newSearchLimitMode) {
                case "0":
                    sendMessage("##config#preset#lower##");
                    break;
                case "1":
                    sendMessage("##config#preset#upper##");
                    break;
                case "2":
                    sendMessage("##config#preset#full##");
                    break;
            }
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (bluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
            Log.d("SettingsFragment", "Bluetooth not connected, not sending message");
            //Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            Log.d("SettingsFragment", "About to send message");
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            bluetoothService.write(send);

        }
    }
}