package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    BluetoothViewModel bluetoothViewModel;

    private final BroadcastReceiver bluetoothStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        changeFragment(TurnOnBluetoothFragment.newInstance());
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        changeFragment(ConnectToBluetoothDeviceFragment.newInstance());
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    private void changeFragment(Fragment newFragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.flFragment, newFragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);

        Toolbar appToolbar = (Toolbar) findViewById(R.id.topAppBar);
        setSupportActionBar(findViewById(R.id.topAppBar));

        appToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_bluetooth:
                    changeFragment(ConnectToBluetoothDeviceFragment.newInstance());
                    return true;
                case R.id.action_subtitles:
                    changeFragment(SubtitlesFragment.newInstance());
                    return true;
                case R.id.action_settings:
                    changeFragment(new SettingsFragment());
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        });

        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothStateReceiver, filter);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            System.err.println("BLUETOOTH NOT SUPPORTED");
        }

        BluetoothService bluetoothService = new BluetoothService(bluetoothHandler);
        bluetoothViewModel = new ViewModelProvider(this).get(BluetoothViewModel.class);
        bluetoothViewModel.setBluetoothService(bluetoothService);

        if (!bluetoothAdapter.isEnabled()) {
            changeFragment(TurnOnBluetoothFragment.newInstance());
        }
        else if (savedInstanceState == null) {
            changeFragment(ConnectToBluetoothDeviceFragment.newInstance());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        BluetoothService bs = bluetoothViewModel.getBluetoothService();
        if (bs != null) {
            bs.stop();
        }
        // Unregister broadcast listeners
        unregisterReceiver(bluetoothStateReceiver);
    }

    /**
     * The Handler that gets information back from the BluetoothService
     */
    private final Handler bluetoothHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                // TODO: Clean up/remove unnecessary code inside switch.
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            changeFragment(SubtitlesFragment.newInstance());
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            // Not implemented
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            // Not implemented
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    // Here sent messages sent from app can be accessed
                    // Not implemented
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    String[] messages = readMessage.split("\n");
                    for(String message : messages) {
                        if(message.contains("##sub##")) {
                            String subtitles = readMessage.split("##sub##")[1];
                            bluetoothViewModel.updateTTSText(subtitles);
                        }
                    }
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    String mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    //if (null != activity) {
                        Toast.makeText(getApplicationContext(), "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    //}
                    break;
                case Constants.MESSAGE_TOAST:
                    //if (null != activity) {
                        Toast.makeText(getApplicationContext(), msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    //}
                    break;
            }
        }
    };
}