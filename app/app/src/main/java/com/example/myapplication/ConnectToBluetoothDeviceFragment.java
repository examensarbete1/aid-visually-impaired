package com.example.myapplication;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.button.MaterialButtonToggleGroup;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConnectToBluetoothDeviceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnectToBluetoothDeviceFragment extends Fragment {

    ListView listView;
    BluetoothAdapter bluetoothAdapter;
    View view;
    BluetoothViewModel bluetoothViewModel;
    BluetoothService bluetoothService;

    private ArrayList<DeviceItem> devices;

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver bluetoothFoundReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                if(deviceName != null && deviceHardwareAddress != null) {
                    DeviceItem di = new DeviceItem(device);
                    if(!devices.contains(di)) {
                        devices.add(di);
                        displayDevices();
                    }
                }
            }
        }
    };

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ConnectToBluetoothDeviceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConnectToBluetoothDeviceFragment newInstance() {
        return new ConnectToBluetoothDeviceFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //get access to location permission
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bluetoothViewModel = new ViewModelProvider(requireActivity()).get(BluetoothViewModel.class);
        bluetoothService = bluetoothViewModel.getBluetoothService();
        if(BluetoothAdapter.getDefaultAdapter().isEnabled()) {

            bluetoothService.start();
        } else {
            changeFragment(TurnOnBluetoothFragment.newInstance());
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED  ){
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        devices = new ArrayList<>();

        view = inflater.inflate(R.layout.fragment_connect_to_bluetooth_device, container, false);

        listView = view.findViewById(R.id.deviceListView);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            DeviceItem device = (DeviceItem) listView.getItemAtPosition(position);

            bluetoothService.connect(device.getDevice(), true);
        });

        MaterialButtonToggleGroup materialButtonToggleGroup = view.findViewById(R.id.scanForDevicesButtonGroup);

        materialButtonToggleGroup.addOnButtonCheckedListener((buttonToggleGroup, checkedId, isChecked) -> {
            Button b = view.findViewById(checkedId);
            // Register for broadcasts when a device is discovered. 1 / 2
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            if (isChecked) {
                // The toggle is enabled
                b.setText(R.string.scanning_for_devices);
                devices.clear();
                // Register for broadcasts when a device is discovered. 2 / 2
                requireActivity().registerReceiver(bluetoothFoundReceiver, filter);
                if(bluetoothAdapter.isDiscovering()) {
                    bluetoothAdapter.cancelDiscovery();
                }
                bluetoothAdapter.startDiscovery();
            } else {
                // The toggle is disabled
                b.setText(R.string.scan_for_devices);
                requireActivity().unregisterReceiver(bluetoothFoundReceiver);
                if(bluetoothAdapter.isDiscovering()) {
                    bluetoothAdapter.cancelDiscovery();
                }
            }
        });

        displayDevices();

        return view;
    }

    private void changeFragment(Fragment newFragment) {
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.flFragment, newFragment)
                .commit();
    }

    private void displayDevices() {
        listView.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, devices));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // Don't forget to unregister the ACTION_FOUND receiver.
        if(bluetoothAdapter.isDiscovering()) {
            // Putting the unregister here might be bad.
            requireActivity().unregisterReceiver(bluetoothFoundReceiver);
            bluetoothAdapter.cancelDiscovery();
        }
    }
}

class DeviceItem{

    private final BluetoothDevice device;

    public DeviceItem(BluetoothDevice device) {
        this.device = device;
    }

    public String getDeviceName() {
        return this.device.getName();
    }

    public String getMacAddress() {
        return this.device.getAddress();
    }

    public BluetoothDevice getDevice() {
        return this.device;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj != null) {
            if(obj instanceof DeviceItem) {
                DeviceItem other = (DeviceItem) obj;
                return getDeviceName().equals(other.getDeviceName()) && getMacAddress().equals(other.getMacAddress());
            }
        }
        return false;
    }

    @NonNull
    @Override
    public String toString() {
        return getDeviceName() + "\n" + getMacAddress();
    }
}