package com.example.myapplication;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TurnOnBluetoothFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TurnOnBluetoothFragment extends Fragment {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ConnectBluetoothFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TurnOnBluetoothFragment newInstance() {
        return new TurnOnBluetoothFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_turn_on_bluetooth, container, false);

        Button turnOnBluetoothButton = view.findViewById(R.id.turnOnBluetoothButton);
        turnOnBluetoothButton.setOnClickListener(new View.OnClickListener() {
            private static final int REQUEST_ENABLE_BT = 1;

            @Override
            public void onClick(View v) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        });

        return view;
    }
}