package com.example.myapplication;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubtitlesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubtitlesFragment extends Fragment {

    String text;
    TextView tv;
    TextToSpeech tts;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment subtitlesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SubtitlesFragment newInstance() {
        return new SubtitlesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BluetoothViewModel bvm = new ViewModelProvider(requireActivity()).get(BluetoothViewModel.class);

        if(bvm.getBluetoothService().getState() != BluetoothService.STATE_CONNECTED) {
            changeFragment(ConnectToBluetoothDeviceFragment.newInstance());
        }

        bvm.getTTSText().observe(getViewLifecycleOwner(), TTSText -> {
            tv.setText(TTSText);
            ConvertTextToSpeech();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subtitles, container, false);

        tv = view.findViewById(R.id.subtitleTextView);
        tts = new TextToSpeech(getContext().getApplicationContext(), status -> {
            if(status == TextToSpeech.SUCCESS) {
                Locale sweLoc = Locale.forLanguageTag("sv");
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                int speechRate = sharedPref.getInt(getString(R.string.speechRateKey), 1);
                tts.setSpeechRate(speechRate/10f);

                int result = tts.setLanguage(sweLoc);
                if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e("error", "This language is not supported");
                }
                else {
                    ConvertTextToSpeech();
                }
            }
            else {
                Log.e("error", "TTS Initialization Failed");
            }
        });

        return view;
    }

    private void changeFragment(Fragment newFragment) {
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.flFragment, newFragment)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tts.shutdown();
    }

    private void ConvertTextToSpeech() {
        text = tv.getText().toString();
        if("".equals(text)) {
            text = "Content not available";
        }
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "whatToWriteHere");
    }
}