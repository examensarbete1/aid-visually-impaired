package com.example.myapplication;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BluetoothViewModel extends ViewModel {
    private MutableLiveData<BluetoothService> bluetoothService;
    private MutableLiveData<String> TTSText;

    public BluetoothService getBluetoothService() {
        if(bluetoothService == null) {
            bluetoothService = new MutableLiveData<>();
        }
        return bluetoothService.getValue();
    }

    public void setBluetoothService(BluetoothService newBluetoothService) {
        if(bluetoothService == null) {
            bluetoothService = new MutableLiveData<>();
        }
        bluetoothService.setValue(newBluetoothService);
    }

    public LiveData<String> getTTSText() {
        if(TTSText == null) {
            TTSText = new MutableLiveData<>();
            TTSText.setValue("NO TEXT AVAILABLE");
        }
        return TTSText;
    }

    public void updateTTSText(String newTTSText) {
        if(TTSText == null) {
            TTSText = new MutableLiveData<>();
        }
        TTSText.setValue(newTTSText);
    }


}
