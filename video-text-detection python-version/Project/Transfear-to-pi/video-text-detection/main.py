import pytesseract
import cv2
import time
from text_detection_video_thread import *
from decorators import decTime
import text_filter
import configreader
import numpy as np
import traceback
import bluetooth_server

# pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
MAINLOOPTRACEBACK = True

def applyFiltering(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # median blurring should be done to remove noise, has to be an odd number
    gray = cv2.medianBlur(gray, 3)

    # # apply thresholding to preprocess the image
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # add white border
    gray = cv2.copyMakeBorder(gray, 10, 10, 10, 10, cv2.BORDER_CONSTANT, None, [255, 255, 255])
    
    h, w = gray.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    
    gray = cv2.bitwise_not(gray)
    # Floodfill from point (0, 0)
    cv2.floodFill(gray, mask, (0,0), 255)

    return gray

# Extract text from imagexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def imageToText(image):
    filtImage = applyFiltering(image)
    subtitle = (pytesseract.image_to_string(filtImage, lang="swe"))
    thread.resume()
    return subtitle, filtImage

# THREADING 2 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# Start bluetooth connection and wait untill connected
bluetooth_instance = bluetooth_server.bluetooth_connection()
while not bluetooth_instance.establish_connection():
    time.sleep(0.25)

# IMPRO :: function should be able to take "boundingbox" input(Static datatype because of multiple threads).
# IMPRO :: webcam funkar inte längre (för Simon)
thread = TextLocalizerThread(configreader.get_east_path(), minConfidence=0.8, videoSource=configreader.get_video_path())
thread.daemon=True
thread.start()

text_filter.init_dict(bluetooth_instance)



# IMPRO :: make into a separate function. call from text_detextion_video_thread when the first image ready instead of bussy waiting
# IMPRO :: pace text_detextion_video_thread to only fetch new frames when this while-loop have gone through an itteration (does time.sleep work with our video time/pacing code)
while thread.is_alive():
    try:
        if thread.is_alive():
            image = thread.getLatestFrame()
            if image is not None:
                if(len(image) > 0):
                    (text, filteredImage) = imageToText(image)
                    # cv2.imshow("Filtered frame", filteredImage)
                    text_filter.splitIntoWords(text)
                    # cv2.waitKey(1)
                else:
                    cv2.waitKey(1)
            else:
                time.sleep(0.1)
        else:
            break
    except Exception as e:
        print("Something happend when trying to extract the subtitle from the image")
        if MAINLOOPTRACEBACK:
            traceback.print_exc()
    finally:
        thread.resume()

bluetooth_instance.close_connection()
exit()
    
# xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
