from difflib import SequenceMatcher
from sys import flags
from decorators import decTime
import time
import threading
import enchant
import configreader
import re

# Debugging xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
DEBUG_ALL = False
DEBUG_SAVE = False
DEBUG_LOAD = False
DEBUG_DICTIONARY = False

def log(message, flag='all'):
    if DEBUG_ALL:
        print(f'all: {message}')

    elif DEBUG_SAVE and flag == 'save':
        print(f'save: {message}')

    elif DEBUG_LOAD and flag == 'load':
        print(f'load: {message}')
    
    elif DEBUG_DICTIONARY and flag == 'dict':
        print(f'dict: {message}')

# Var xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
bluetooth = None
# Dictionary xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

dictionary = None

# Unknown "saved" words
unknownWordsFromFile = set()
# Unknown not saved words
new_unknown_words = set()


# Load saved unknown words
def __load_unknown_set():
    global unknownWordsFromFile
    if(len(unknownWordsFromFile) == 0):
        try:
            with open(configreader.get_dict_unknownword_path(), 'r', encoding='utf-8') as f:
                unknownWordsFromFile = set(f.read().splitlines())
            
        except:
            log("Could not find unknown Words", 'load')

# save set to disk in set interval
def __save_unknown_words(interval):
    while(True):
        time.sleep(interval)
        log("running save thread", 'save')

        if(len(new_unknown_words) > 0):
            with open(configreader.get_dict_unknownword_path(), 'a', encoding='utf-8') as f:

                for x in range(len(new_unknown_words)):
                    unknownWord = new_unknown_words.pop()
                    if unknownWord not in unknownWordsFromFile:
                        f.write(unknownWord + "\n")
                        unknownWordsFromFile.add(unknownWord)
                        log(f"'{unknownWord}' added to unkown words", 'save')
                    else:
                        log(f"'{unknownWord}' already in file", 'save')


def testFunc():
    print("Running every 10:th second")

# returns a list with words in dictionary. Words that don't exist will be saved to eventually be added
def existingWords(wordList):
    pass

# returns True if word in enchant dictionary else False and is added to new_unknown_words
def wordInDictionary(word):
    if dictionary.check(word):
        return True
    else:
        log(f"'{word}' not in dictionary, adding to new unknown words", 'dict')
        new_unknown_words.add(word)
        return False


# @decTime
def init_dict(bluetooth_instance):
    log("initializing dictionary")
    global dictionary
    global old_text_similarity_threshold
    global bluetooth
    if not (enchant.Broker().dict_exists(configreader.get_enchant_dict())):
        raise Exception("Could not find dictionary. Make sure the dictionary mentioned in config.cfg in installed correctly. Installation instrictions can be found in /resurser")
    dictionary = enchant.Dict(configreader.get_enchant_dict())
    configreader.init_customWords(dictionary)
    __load_unknown_set()
    bluetooth = bluetooth_instance
    save_unknown_thread = threading.Thread(target=__save_unknown_words, args=(30,), daemon=True)
    save_unknown_thread.start()


# text similarity xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

def list_to_string(_list_):
    temp = str(" ").join(_list_)
    if len(temp) > 0:
        temp += "\n"
    return temp


    


# determines how similar two string are and returns the ratio
# give input a better name than 'a' and 'b'
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def stabilize_subtitle(minConfidence):
    """Return a list where each word is above a sertain score. The treshhold is calculated based on
        a pertentage(minConfidence) of the highest score"""
    global foundWords
    ## find highest scored word and multiply it with minConfidence ##
    highest_scored_word = 0
    for score in foundWords.values():
        if(highest_scored_word < score):
            highest_scored_word = score
    # Point limit is the lowest accepted score from words taken from the
    point_limit = None
    if(highest_scored_word == 1):
        return list()
    else:
        point_limit = highest_scored_word*(minConfidence) 
    ## Pick out the words with the scores equal or  ##
    stabilized_list = list()
    for keyword in foundWords.keys():
        if(foundWords[keyword] >= point_limit):
            stabilized_list.append(keyword)
    return stabilized_list

# used to compare previous interation of text
old_text_buffer_size = configreader.get_frame_to_frame_buffer_size() 
oldTexts = []
old_text_similarity_threshold = configreader.get_buffer_similarity()
# words found with the amount of times found over one piece of subtitles
foundWords = {}
__have_printed = False

def frame_to_frame_stabilizer(text):
    global oldTexts
    global foundWords
    global __have_printed

    # Have atleast 1 picture to compare with before going further in the code
    if(len(oldTexts) < 1): 
        oldTexts.append(list(text))
        return

    # Calculate an increase in the threshold if all buffer slots are not filled. The more slots that are filled, the lower the increase
    # If all slots are filled, decrementalDecrease will be 0
    decrementalDecrease = ((0.9 - old_text_similarity_threshold)/(old_text_buffer_size-1)) * ((old_text_buffer_size) - len(oldTexts))
    
    # If the found text is similar enough to the previous frames, add the newly found text to a bufferslot
    # Otherwise print the found text and reset the variables
    for old_text in oldTexts:
        if(similar(text, old_text) >= (old_text_similarity_threshold + decrementalDecrease)):
            if(len(oldTexts) < old_text_buffer_size): # If the buffer isn't full, add found text
                oldTexts.append(list(text))
            else: # if it is full, print the found (stabelized) text
                if not (__have_printed):
                    stabilized_subtitle = stabilize_subtitle(configreader.get_wordscore_threshold())
                    bluetooth.send(list_to_string(stabilized_subtitle))
                    print(f"Stabelized text: {stabilized_subtitle}")
                    __have_printed = True
            return
        else: # if the text isn't similar (new text), print and then clear data
            if not (__have_printed):
                stabilized_subtitle = stabilize_subtitle(configreader.get_wordscore_threshold())
                bluetooth.send(list_to_string(stabilized_subtitle))
                print(f"Stabelized text: {stabilized_subtitle}")
            foundWords = {}
            oldTexts = []
            __have_printed = False
    return

def splitIntoWords(text):
    global oldTexts
    global foundWords
    # if text and old text is not similar, then text is from a new sentence and found words is reset
    
    # remove all non-word and non-whitespace characters
    text = re.sub("[^A-Za-zÅÄÖåäö\\s]", "", text, flags=re.MULTILINE)

    # split text on whitespaces
    for word in text.split():
        word = word.lower()
        if wordInDictionary(word):
            
            if(word not in foundWords):
                foundWords[word] = 0

            foundWords[word] = foundWords[word] + 1

    frame_to_frame_stabilizer(text)
    # print(len(oldTexts))

    