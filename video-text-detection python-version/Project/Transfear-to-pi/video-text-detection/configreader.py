import configparser
import os


#Create configtemplate if theres no configfile
if not (os.path.exists("config.cfg")):
    with open("config.cfg", 'w', encoding='utf-8') as f:
        f.write(r"""
[Paths]
video_path = ...
east_path = resurser\frozen_east_text_detection.pb
dict_unknown_words = video-text-detection\Dict\unknownWords.txt

#All numbers range from 0 - 1
[TextDetection]
enchant_dict = sv_SE
# v not yet implemented v
VideoSearchLimitX1 = 0
VideoSearchLimitY1 = 0.6
VideoSearchLimitX2 = 1
VideoSearchLimitY2 = 1
# ^ not yet implemented ^
# How many of the previous frames the current text should be compared to.
# At least one of the previous frames must match to (buffersimilarity*100) % to pass)
# Can take values between 0.01 - 0.89
BufferSimilarity = 0.55
# accepted values 2 - 10
FrameToFrameBufferSize = 2
# How many percentage of the highest scored word each word must have to "pass"
WordScoreTreshhold = 0.3
        """)
    print('config.cfg created in repo\'s root dicectory. Replace "..." with your paht(s)')
    exit(0)

config = configparser.ConfigParser()
config.read('config.cfg')

def get_video_path():
    path = __get('Paths','video_path')
    if (os. path.exists(path)):
        return path
    else:
        raise Exception(f"Could not find the the videofile at given path\nPath: {path}")

def get_east_path():
    return __get('Paths', 'east_path')

def get_dict_unknownword_path():
    return __get('Paths', 'dict_unknown_words')

def get_enchant_dict():
    return __get('TextDetection', 'enchant_dict')

def get_video_searchlimit():
    x1 = float(__get('TextDetection', 'VideoSearchLimitX1'))
    x2 = float(__get('TextDetection', 'VideoSearchLimitX2'))
    y1 = float(__get('TextDetection', 'VideoSearchLimitY1'))
    y2 = float(__get('TextDetection', 'VideoSearchLimitY2'))    
    return (x1, y1, x2, y2)

def get_buffer_similarity():
    return float(__get('TextDetection', 'BufferSimilarity'))

def get_frame_to_frame_buffer_size():
    return int(__get('TextDetection', 'FrameToFrameBufferSize'))

def get_wordscore_threshold():
    return float(__get('TextDetection', 'WordScoreTreshhold'))

def __get(section, part):
    try:
        return config[section][part]
    except:
        raise Exception(f"could not find item in configfile. \nSection: {section} \nPart: {part}")


########################## Custom words, Dictionary ##################################
if not (os.path.exists("Transfear-to-pi/video-text-detection/Dict/customDictionaryWords.txt")):
    with open("Transfear-to-pi/video-text-detection/Dict/customDictionaryWords.txt", 'w', encoding='utf-8') as f:
        f.write("#Write a list with words to be added to the dictionary(remove this line)\n")
        f.write("#The words will be case sensitive")
    
# Call from text_filter
def init_customWords(dictionary):
    try:
        with open('Transfear-to-pi/video-text-detection/Dict/customDictionaryWords.txt', 'r', encoding='utf-8') as f:
            custom_words = set(f.read().splitlines())
    except:
        pass
        
    for word in custom_words:
        if not (word.__contains__('#')):
            dictionary.add_to_session(word)
      