import functools
import time

def decTime(func):
    functools.wraps(func)
    def wrapper(*args, **kwargs):
        timeStart = time.time()
        returnData = func(*args, **kwargs)
        print(f"function: {func.__name__} ran {time.time()-timeStart:.3f} seconds")
        return returnData
    return wrapper