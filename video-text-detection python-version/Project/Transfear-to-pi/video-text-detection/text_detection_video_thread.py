from imutils.video import FPS
from imutils.video import VideoStream
from imutils.object_detection import non_max_suppression
import numpy as np
import imutils
import time
import cv2
import threading
from threading import Thread
from threading import Lock
from search_limitation import searchLimitation
from decorators import decTime
import bluetooth_server

class TextLocalizerThread(Thread):
	def __init__(self, eastSource, minConfidence, videoSource=None):
		super(TextLocalizerThread, self).__init__()
		self.eastSource = eastSource
		self.minConfidence = minConfidence
		self.videoSource = videoSource
		self.threadLock = Lock()
		self.latestLocalization = None
		self.__text_localizationloop_running = threading.Event()
		self.__text_localizationloop_running.set()

	def run(self):
		self.detectTextInVideo(self.eastSource, self.minConfidence, self.videoSource)

	def getLock(self):
		return self.threadLock

	def resume(self):
		self.__text_localizationloop_running.set()
	
	def pause(self):
		self.__text_localizationloop_running.clear()

	def getLatestFrame(self):
		try:
			with self.threadLock:
				if len(self.latestLocalization) == 0:
					pass
				returnPic = self.latestLocalization.copy()
		except:
			return None
		return returnPic

	def decode_predictions(self, scores, geometry, minConfidence):
		# grab the number of rows and columns from the scores volume, then
		# initialize our set of bounding box rectangles and corresponding
		# confidence scores
		(numRows, numCols) = scores.shape[2:4]
		rects = []
		confidences = []

		# loop over the number of rows
		for y in range(0, numRows):
			# extract the scores (probabilities), followed by the
			# geometrical data used to derive potential bounding box
			# coordinates that surround text
			scoresData = scores[0, 0, y]
			xData0 = geometry[0, 0, y]
			xData1 = geometry[0, 1, y]
			xData2 = geometry[0, 2, y]
			xData3 = geometry[0, 3, y]
			anglesData = geometry[0, 4, y]

			# loop over the number of columns
			for x in range(0, numCols):
				# if our score does not have sufficient probability,
				# ignore it
				if scoresData[x] < minConfidence:
					continue

				# compute the offset factor as our resulting feature
				# maps will be 4x smaller than the input image
				(offsetX, offsetY) = (x * 4.0, y * 4.0)

				# extract the rotation angle for the prediction and
				# then compute the sin and cosine
				angle = anglesData[x]
				cos = np.cos(angle)
				sin = np.sin(angle)
				
				# use the geometry volume to derive the width and height
				# of the bounding box
				h = xData0[x] + xData2[x]
				w = xData1[x] + xData3[x]

				# compute both the starting and ending (x, y)-coordinates
				# for the text prediction bounding box
				endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
				endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
				startX = int(endX - w)
				startY = int(endY - h)

				# add the bounding box coordinates and probability score
				# to our respective lists
				rects.append((startX, startY, endX, endY))
				confidences.append(scoresData[x])

		# return a tuple of the bounding boxes and associated confidences
		return (rects, confidences)


	def detectTextInVideo(self, eastSource, minConfidence=0.5, videoSource=None, width=320, height=320):
		# initialize the original frame dimensions, new frame dimensions,
		# and ratio between the dimensions
		(W, H) = (None, None)
		(newW, newH) = (width, height)
		(rW, rH) = (None, None)

		# define the two output layer names for the EAST detector model that
		# are interesting. The first is the output probabilities and the
		# second can be used to derive the bounding box coordinates of text
		layerNames = [
			"feature_fusion/Conv_7/Sigmoid",
			"feature_fusion/concat_3"]

		# load the pre-trained EAST text detector
		print("[INFO] loading EAST text detector...")
		net = cv2.dnn.readNet(eastSource)

		# if a video path was not supplied, grab the reference to the web cam
		if videoSource is None:
			print("[INFO] geting webcam video stream...")
			vs = cv2.VideoCapture(0)
			time.sleep(1.0)
			
		# otherwise, grab a reference to the video file
		else:
			vs = cv2.VideoCapture(videoSource)

		# start the FPS throughput estimator
		fps = FPS().start()
		
		# get video framerate (used to pace video-playback)
		fpsVal = vs.get(cv2.CAP_PROP_FPS)
		print(f"Video fps: {fpsVal}")

		lastTime = time.time()

		# Define "searchbox"
		searchBox = searchLimitation(0, 0.6, 1, 1)
		# loop over frames from the video stream
		while True:
			self.__text_localizationloop_running.wait()
			# Get and crop next frame. Operation takes time into account. If longer time has passsed, 
			# frames will be skipped to keep video-playback at realtime
			timeSinceLastItteration = time.time() - lastTime
			for x in range(int(timeSinceLastItteration * fpsVal)-1):
				vs.read()
			frame = vs.read()
			if(frame[1] is None):
				print("Video ended")
				break
			searchBox.SetImage(frame[1])
			frame = searchBox.getCropImage()
			lastTime = time.time()

			# Keep the original frame
			orig = frame.copy()

			# if our frame dimensions are None, we still need to compute the
			# ratio of old frame dimensions to new frame dimensions
			if W is None or H is None:
				(H, W) = frame.shape[:2]
				rW = W / float(newW)
				rH = H / float(newH)

			# resize the frame, this time ignoring aspect ratio
			frame = cv2.resize(frame, (newW, newH))

			# construct a blob from the frame and then perform a forward pass
			# of the model to obtain the two output layer sets
			blob = cv2.dnn.blobFromImage(frame, 1.0, (newW, newH),
				(123.68, 116.78, 103.94), swapRB=True, crop=False)
			net.setInput(blob)
			(scores, geometry) = net.forward(layerNames)

			# decode the predictions, then  apply non-maxima suppression to
			# suppress weak, overlapping bounding boxes
			(rects, confidences) = self.decode_predictions(scores, geometry, minConfidence)
			boxes = non_max_suppression(np.array(rects), probs=confidences)

			# compute the boundingboxes for the subtitle
			minX = None
			minY = None
			maxX = None 
			maxY = None

			# loop over the bounding boxes
			for (startX, startY, endX, endY) in boxes:
				# scale the bounding box coordinates based on the respective ratios
				startX = int(startX * rW)
				startY = int(startY * rH)
				endX = int(endX * rW)
				endY = int(endY * rH)

				if minX is None or startX < minX:
					minX = startX
				if minY is None or startY < minY:
					minY = startY

				if maxX is None or endX > maxX:
					maxX = endX
				if maxY is None or endY > maxY:
					maxY = endY

				# draw the bounding box on the frame
				# cv2.rectangle(orig, (startX, startY), (endX, endY), (0, 255, 0), 2)

			# crop image acording to boudningboxes found above
			crop_image = orig.copy()
			if minX is not None:
				# Setting croping boundaries
				y = self.clamp((minY), 0, ((H*rH)-1)) - 20
				h = self.clamp((maxY), 0, ((H*rH)-1)) + 20
				x = self.clamp((minX), 0, ((W*rW)-1)) - 20
				w = self.clamp((maxX), 0, ((W*rW)-1)) + 20

				crop_image = crop_image[y:h, x:w]

			# update the FPS counter
			fps.update()

			# show the output frame
			with self.threadLock:
				self.latestLocalization = crop_image.copy()
			# cv2.imshow("Text Detection", orig)
			# key = cv2.waitKey(1) & 0xFF

			# if the `q` key was pressed, break from the loop
			# if key == ord("q"):
			# 	break

			self.pause()

		# stop the timer and display FPS information
		fps.stop()
		print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
		print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

		# if we are using a webcam, release the pointer
		# if videoSource is not None:
		# 	vs.stop()
		# # otherwise, release the file pointer
		# else:
		vs.release()

		# close all windows
		cv2.destroyAllWindows()

	def clamp(self, val, mini, maxi):
		return int(max(mini, min(val, maxi)))